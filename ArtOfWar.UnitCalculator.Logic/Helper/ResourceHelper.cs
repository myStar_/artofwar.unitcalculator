﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;

namespace ArtOfWar.UnitCalculator.Logic.Helper
{
    public static class ResourceHelper
    {
        //public static bool ResourceExists(string resourcePath)
        //{
        //    var assembly = Assembly.GetExecutingAssembly();

        //    return ResourceExists(assembly, resourcePath);
        //}

        public static bool ResourceExists(Assembly assembly, string resourcePath)
        {
            return GetResourcePaths(assembly).Any(x => resourcePath.ToLowerInvariant().EndsWith(x));
        }

        private static IEnumerable<string> GetResourcePaths(Assembly assembly)
        {
            var culture = System.Threading.Thread.CurrentThread.CurrentCulture;
            var resourceName = assembly.GetName().Name + ".g";
            var resourceManager = new ResourceManager(resourceName, assembly);

            try
            {
                var resourceSet = resourceManager.GetResourceSet(culture, true, true);

                foreach (System.Collections.DictionaryEntry resource in resourceSet)
                {
                    yield return resource.Key.ToString();
                }
            }
            finally
            {
                resourceManager.ReleaseAllResources();
            }
        }
    }
}
