﻿using ArtOfWar.UnitCalculator.Logic.Service.Contracts;

namespace ArtOfWar.UnitCalculator.Logic.Service
{
    public class GameSettings : ISettings
    {
        public bool Level10Enabled { get; set; }

        public GameSettings()
        {
            this.Level10Enabled = false;
        }
    }
}
