﻿using System.Linq;
using System.Threading.Tasks;
using GoogleTranslateFreeApi;

namespace ArtOfWar.UnitCalculator.Logic.Service
{
    /// <summary>
    /// Using https://github.com/Grizley56/GoogleTranslateFreeApi
    /// (because Google-Translation needs Auth with some Json Key...)
    /// </summary>
    public class GoogleFreeTranslator : Contracts.ITranslator
    {
        private readonly GoogleTranslator googleTranslator;
        private readonly Language languageFrom;
        private readonly Language languageTo;

        public GoogleFreeTranslator()
        {
            this.googleTranslator = new GoogleTranslator();

            this.languageFrom = Language.English;
            this.languageTo = Language.German;
        }

        // If there are too many Requests from one IP, its getting blocked.
        // => There is a need of using a Proxy
        private void SwitchProxy()
        {
            //this.googleTranslator.Proxy = new WebProxy("url");
        }

        public string Translate(string input)
        {
            this.SwitchProxy();

            var result = this.googleTranslator.TranslateLiteAsync(input, this.languageFrom, this.languageTo).GetAwaiter().GetResult();

            return result.FragmentedTranslation.FirstOrDefault();
        }

        public async Task<string> TranslateASync(string input)
        {
            this.SwitchProxy();

            var result = await this.googleTranslator.TranslateLiteAsync(input, this.languageFrom, this.languageTo);

            return result.FragmentedTranslation.FirstOrDefault();
        }
    }
}
