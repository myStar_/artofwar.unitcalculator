﻿using System.Collections.Generic;
using ArtOfWar.UnitCalculator.Model.Base.Contracts;

namespace ArtOfWar.UnitCalculator.Logic.Service.Contracts
{
    public interface IUnitService
    {
        IEnumerable<IUnitModel> GetAllUnits();
    }
}