﻿namespace ArtOfWar.UnitCalculator.Logic.Service.Contracts
{
    public interface ISettings
    {
        bool Level10Enabled { get; set; }
    }
}
