﻿using System.Threading.Tasks;

namespace ArtOfWar.UnitCalculator.Logic.Service.Contracts
{
    public interface ITranslator
    {
        string Translate(string input);
        Task<string> TranslateASync(string input);
    }
}