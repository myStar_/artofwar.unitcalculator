﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ArtOfWar.UnitCalculator.Logic.Service.Contracts;
using ArtOfWar.UnitCalculator.Model.Base.Contracts;
using ArtOfWar.UnitCalculator.Model.Base.Enums;
using log4net;

namespace ArtOfWar.UnitCalculator.Logic.Service
{
    public class HardCodedUnitService : IUnitService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly ISettings settings;

        public HardCodedUnitService(ISettings settings = null)
        {
            this.settings = settings;

            if (settings == null)
            {
                this.settings = new GameSettings();
            }
        }


        public IEnumerable<IUnitModel> GetAllUnits()
        {
            Log.Debug("Getting UnitList...");

            var assemblyName = "ArtOfWar.UnitCalculator.Model";

            var modelAssemly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.GetName().Name.Equals(assemblyName));

            var types = modelAssemly.GetTypes().Where(x => typeof(IUnitModel).IsAssignableFrom(x)
                                                        && !x.IsInterface
                                                        && x.FullName.StartsWith($"{assemblyName}.Units"));

            var unitItems = types.Select(x => (IUnitModel)Activator.CreateInstance(x));


            if (!this.settings.Level10Enabled)
            {
                foreach (var unit in unitItems)
                {
                    unit.UnitLevels.RemoveAll(x => x.Level.Equals(Level.lvl_10));
                }
            }

            return unitItems;
        }
    }
}
