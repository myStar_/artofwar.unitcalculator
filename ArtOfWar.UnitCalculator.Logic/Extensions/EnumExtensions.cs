﻿using System;
using System.ComponentModel;

namespace ArtOfWar.UnitCalculator.Logic.Extensions
{
    public static class EnumExtensions 
    {
        public static string GetDescriptionAttribute<T>(this T source) where T : Enum
        {
            var fieldInfo = source.GetType().GetField(source.ToString());

            var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }

            return source.ToString();
        }
    }
}
