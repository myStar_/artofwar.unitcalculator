﻿using System.Threading.Tasks;
using ArtOfWar.UnitCalculator.Logic.Service;
using ArtOfWar.UnitCalculator.Logic.Service.Contracts;
using NUnit.Framework;

namespace ArtOfWar.UnitCalculator.Tests.ArtOfWar.UnitCalculator.Logic.Service
{
    [TestFixture]
    public class GoogleFreeTranslatorTests
    {
        private ITranslator sut;

        [SetUp]
        public void SetUp()
        {
            this.sut = new GoogleFreeTranslator();
        }

        [Test]
        public void Translate_Hallo_ShouldReturnHallo()
        {
            var input = "Hello";

            var output = this.sut.Translate(input);

            Assert.That(output, Is.EqualTo("Hallo"));
        }

        [Test]
        public async Task TranslateAsync_Hallo_ShouldReturnHallo()
        {
            var input = "Hello";

            var output = await this.sut.TranslateASync(input);

            Assert.That(output, Is.EqualTo("Hallo"));
        }
    }
}
