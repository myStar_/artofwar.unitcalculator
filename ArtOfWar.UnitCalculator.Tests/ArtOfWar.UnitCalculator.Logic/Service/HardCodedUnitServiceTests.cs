﻿using ArtOfWar.UnitCalculator.Logic.Service;
using NUnit.Framework;

namespace ArtOfWar.UnitCalculator.Tests.ArtOfWar.UnitCalculator.Logic.Service
{
    [TestFixture]
    public class HardCodedUnitServiceTests
    {
        [Test]
        public void CreateServiceAndGetItems_IsNotNullAndEmpty()
        {
            var sut = new HardCodedUnitService();

            var items = sut.GetAllUnits();

            Assert.That(items, Is.Not.Null.And.Not.Empty);
        }
    }
}
