﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using ArtOfWar.UnitCalculator.Converter;
using ArtOfWar.UnitCalculator.Model.Base.Enums;
using NUnit.Framework;

namespace ArtOfWar.UnitCalculator.Tests.ArtOfWar.UnitCalculator.Converter
{
    [TestFixture]
    public class UnitLevelToStringConverterTests
    {
        private IValueConverter sut;

        [SetUp]
        public void SetUp()
        {
            this.sut = new UnitLevelToStringConverter();
        }

        private static IEnumerable<Level> LevelGetter()
        {
            return ((Level[])Enum.GetValues(typeof(Level))).Where(x=>x != Level.empty);
        }

        [Test]
        [TestCaseSource("LevelGetter")]
        public void TestConverter_Convert_ShouldRetursLevelString(Level level)
        {
            var result = this.sut.Convert(level, null, null, null);

            Assert.That(result, Is.EqualTo($"Level {(int)level}"));
        }

        [Test]
        public void TestConverter_ConvertFalseDataType_ThrowsArgumentException()
        {
            int intInput = 2;

            Assert.Throws<ArgumentException>(() =>
            {
                var result = this.sut.Convert(intInput, null, null, null);
            });
        }

        [Test]
        public void TestConverter_ConvertBack_ThrowsNotImplementedException()
        {
            var input = 2;

            Assert.Throws<NotImplementedException>(() =>
            {
                var result = this.sut.ConvertBack(input, null, null, null);
            });
        }
    }
}
