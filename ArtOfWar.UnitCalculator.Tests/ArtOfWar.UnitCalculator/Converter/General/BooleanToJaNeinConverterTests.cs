﻿using System;
using System.Windows.Data;
using ArtOfWar.UnitCalculator.Converter.General;
using NUnit.Framework;

namespace ArtOfWar.UnitCalculator.Tests.ArtOfWar.UnitCalculator.Converter.General
{
    [TestFixture]
    public class BooleanToJaNeinConverterTests
    {
        private IValueConverter sut;

        [SetUp]
        public void SetUp()
        {
            this.sut = new NullableBooleanToJaNeinConverter();
        }

        [Test]
        public void TestConverter_ConvertFalseDataType_ThrowsArgumentException()
        {
            int intInput = 2;

            Assert.Throws<ArgumentException>(() =>
            {
                var result = this.sut.Convert(intInput, null, null, null);
            });
        }

        [Test]
        public void TestConverter_Convert_ShouldReturntodo()
        {
            bool? input = null;

            var result = this.sut.Convert(input, null, null, null);

            Assert.That(result, Is.EqualTo("?"));
        }

        [Test]
        public void TestConverter_Convert_ShouldReturnja()
        {
            bool? input = true;

            var result = this.sut.Convert(input, null, null, null);

            Assert.That(result, Is.EqualTo("ja"));
        }

        [Test]
        public void TestConverter_Convert_ShouldReturnnein()
        {
            bool? input = false;

            var result = this.sut.Convert(input, null, null, null);

            Assert.That(result, Is.EqualTo("nein"));
        }

        [Test]
        public void TestConverter_ConvertBack_ThrowsNotImplementedException()
        {
            var input = "ja";

            Assert.Throws<NotImplementedException>(() =>
                {
                    var result = this.sut.ConvertBack(input, null, null, null);
                });
        }
    }
}
