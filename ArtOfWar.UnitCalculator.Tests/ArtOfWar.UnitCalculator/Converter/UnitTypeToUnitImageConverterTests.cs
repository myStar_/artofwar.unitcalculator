﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using ArtOfWar.UnitCalculator.Converter;
using ArtOfWar.UnitCalculator.Model.Base.Enums;
using NUnit.Framework;
using System.Windows.Media.Imaging;

namespace ArtOfWar.UnitCalculator.Tests.ArtOfWar.UnitCalculator.Converter
{
    [TestFixture]
    public class UnitTypeToUnitImageConverterTests
    {
        private IValueConverter sut;

        [SetUp]
        public void SetUp()
        {
            // => Need this, because the "ArtOfWar.UnitCalculator.Tests"-Pack
            //               ist not equal to "ArtOfWar.UnitCalculator"-Pack
            if (!UriParser.IsKnownScheme("pack"))
            {
                new System.Windows.Application();
            }

            this.sut = new UnitTypeToUnitImageConverter();
        }

        /// <summary>
        /// TODO: Implement for all Units [or Insert Images for all Units]
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<UnitType> UnitTypeGetter()
        {
            yield return UnitType.T1_Infanterie;
            //return ((UnitType[])Enum.GetValues(typeof(UnitType))).Where(x => x != UnitType.empty);
        }

        [Test]
        [TestCaseSource("UnitTypeGetter")]
        public void TestConverter_Convert_ShouldRetursLevelString(UnitType unitType)
        {
            var result = this.sut.Convert(unitType, null, null, null);

            if (result == null)
            {
                return;
            }

            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.TypeOf<BitmapImage>());
        }

    }
}
