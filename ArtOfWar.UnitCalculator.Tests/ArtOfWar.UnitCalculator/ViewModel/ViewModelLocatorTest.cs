﻿using ArtOfWar.UnitCalculator.ViewModel;
using NUnit.Framework;

namespace ArtOfWar.UnitCalculator.Tests.ArtOfWar.UnitCalculator
{
    [TestFixture]
    public class ViewModelLocatorTest
    {
        ViewModelLocator sut;

        [SetUp]
        public void Setup()
        {
            this.sut = new ViewModelLocator();
        }

        [Test]
        public void TestNavigation_ShouldPass()
        {
            //var unitService = new HardCodedUnitService();

            //var unitOverviewViewModel = new UnitOverviewViewModel(unitService);
            //var unitCompareViewModel = new UnitCompareViewModel(unitService);
            //var unitGridViewModel = new UnitGridViewModel(unitService);
            //var battlefieldGridViewModel = new BattlefieldGridViewModel(unitService);
            //var unitValueViewModel = new UnitValueViewModel(unitService);

            //Assert.That(this.sut.CurrentViewModel, Is.EqualTo(unitGridViewModel));

            //this.sut.UnitCompareViewModelShowCommand.Execute(null);
            //Assert.That(this.sut.CurrentViewModel, Is.EqualTo(unitCompareViewModel));

            //this.sut.UnitValueViewModelShowCommand.Execute(null);
            //Assert.That(this.sut.CurrentViewModel, Is.EqualTo(unitValueViewModel));

            //this.sut.BattlefieldGridViewModelShowCommand.Execute(null);
            //Assert.That(this.sut.CurrentViewModel, Is.EqualTo(battlefieldGridViewModel));

            //this.sut.UnitOverviewViewModelShowCommand.Execute(null);
            //Assert.That(this.sut.CurrentViewModel, Is.EqualTo(unitOverviewViewModel));

            //this.sut.UnitGridViewModelShowCommand.Execute(null);
            //Assert.That(this.sut.CurrentViewModel, Is.EqualTo(unitGridViewModel));
        }

        //[Test]
        public void TEstFoo()
        {
            //var unitList = new Fixture().CreateMany<UnitModel>(2);

            //var mockShit = new Mock<IUnitService>();
            //mockShit.Setup(x => x.GetUnits()).Returns(unitList);

            //var unitListViewModel = new UnitListViewModel(mockShit.Object);

            //var itemCount = unitListViewModel.Count;

            //var sut = new UnitService().GetUnits();

            //Assert.That(itemCount, Is.EqualTo(sut.Count()));
        }
    }
}
