﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using System.Linq;
using ArtOfWar.UnitCalculator.Model.Base.Enums;
using ArtOfWar.UnitCalculator.Model.Base.Contracts;

namespace ArtOfWar.UnitCalculator.Tests.ArtOfWar.UnitCalculator.Model.Units
{
    [TestFixture]
    public class UnitModelTests
    {
        private IEnumerable<IUnitModel> unitList;

        [SetUp]
        public void Setup()
        {
            var assemblyName = "ArtOfWar.UnitCalculator.Model";

            var modelAssemly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.GetName().Name.Equals(assemblyName));

            var types = modelAssemly.GetTypes().Where(x => typeof(IUnitModel).IsAssignableFrom(x)
                                                        && !x.IsInterface
                                                        && x.FullName.StartsWith($"{assemblyName}.Units"));

            this.unitList = types.Select(x => (IUnitModel)Activator.CreateInstance(x));
        }

        [Test]
        public void TestAllUnitCount_ShouldPass()
        {
            var unitCount = this.unitList.Count();

            Assert.That(unitCount, Is.EqualTo(38));
        }

        [Test]
        public void TestAllUnitsForEnumSets_ShouldPass()
        {
            foreach (var sut in this.unitList)
            {
                Assert.That(sut.UnitType, Is.Not.EqualTo(UnitType.empty));

                Assert.That(sut.Tier, Is.Not.EqualTo(Tier.empty));

                Assert.That(sut.UnitLevels, !Contains.Item(Level.empty));
            }
        }

        [Test]
        public void TestAllUnitsOnlyOnce_ShouldPass()
        {
            var unitTypes = Enum.GetValues(typeof(UnitType)).Cast<UnitType>().Where(x => !x.Equals(UnitType.empty));

            foreach (var sut in unitTypes)
            {
                var unitCountForUnitType = this.unitList.Count(x => x.UnitType.Equals(sut));

                Assert.That(unitCountForUnitType, Is.EqualTo(1));
            }
        }

        [Test]
        public void TestAllUnitValuesSmaller_ShouldPass()
        {
            var unitTypes = Enum.GetValues(typeof(UnitType)).Cast<UnitType>().Where(x => !x.Equals(UnitType.empty));

            foreach (var sut in unitTypes)
            {
                var unitCountForUnitType = this.unitList.Count(x => x.UnitType.Equals(sut));

                Assert.That(unitCountForUnitType, Is.EqualTo(1));
            }
        }
    }
}
