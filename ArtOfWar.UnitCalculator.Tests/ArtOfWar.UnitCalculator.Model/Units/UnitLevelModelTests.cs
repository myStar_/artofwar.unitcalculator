﻿using System;
using System.Collections.Generic;
using System.Linq;
using ArtOfWar.UnitCalculator.Model.Base.Contracts;
using ArtOfWar.UnitCalculator.Model.Base.Enums;
using NUnit.Framework;

namespace ArtOfWar.UnitCalculator.Tests.ArtOfWar.UnitCalculator.Model.Units
{
    [TestFixture]
    public class UnitLevelModelTests
    {
        private IEnumerable<IUnitModel> unitList;

        [SetUp]
        public void Setup()
        {
            var assemblyName = "ArtOfWar.UnitCalculator.Model";

            var modelAssemly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.GetName().Name.Equals(assemblyName));

            var types = modelAssemly.GetTypes().Where(x => typeof(IUnitModel).IsAssignableFrom(x)
                                                        && !x.IsInterface
                                                        && x.FullName.StartsWith($"{assemblyName}.Units"));

            this.unitList = types.Select(x => (IUnitModel)Activator.CreateInstance(x));
        }

        [Test]
        public void TestAllUnitsForLevels_ShouldPass()
        {
            foreach (var sut in this.unitList)
            {
                Assert.That(sut.UnitLevels.Count, Is.EqualTo(10));
            }
        }

        [Test]
        public void TestAllUnitsValuesNotSmallerInNextLevel_ShouldPass()
        {
            foreach (var sut in this.unitList)
            {
                foreach (var level in sut.UnitLevels.Where(x => x.Level < Level.lvl_10))
                {
                    var nextLevel = sut.UnitLevels[sut.UnitLevels.IndexOf(level) + 1];

                    try
                    {
                        Assert.That(level.HP, Is.LessThanOrEqualTo(nextLevel.HP));

                        Assert.That(level.Offense, Is.LessThanOrEqualTo(nextLevel.Offense));

                        Assert.That(level.Defense, Is.LessThanOrEqualTo(nextLevel.Defense));

                        Assert.That(level.TroopCount, Is.LessThanOrEqualTo(nextLevel.TroopCount));
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }
        }
    }
}
