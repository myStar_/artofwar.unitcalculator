﻿using System.Collections.Generic;
using ArtOfWar.UnitCalculator.Model.Base.Contracts;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Base
{
    public  class UnitModel : IUnitModel
    {
        public UnitType UnitType { get; set; }

        public Tier Tier { get; set; }

        public Skill Skill { get; set; }

        public bool? HasAreaOfEffect { get; set; }

        public double Attackspeed { get; set; }

        public List<UnitLevelModel> UnitLevels { get; set; }
        
        public UnitModel()
        {
            this.UnitLevels = new List<UnitLevelModel>();
        }
    }
}
