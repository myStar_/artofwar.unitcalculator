﻿using System;
using ArtOfWar.UnitCalculator.Model.Base.Contracts;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Base
{
    public class UnitLevelModel : IUnitLevelModel
    {
        private readonly IUnitModel unit;

        public Level Level { get; }

        public int HP { get; set; }

        public int Offense { get; set; }

        public double DPS => Math.Round(this.unit.Attackspeed * this.Offense);

        public int Defense { get; set; }

        public int TroopCount { get; set; }

        public UnitLevelModel(IUnitModel unit, Level unitLevel, int hp, int offense, int defense, int troopCount)
        {
            this.unit = unit;

            this.Level = unitLevel;

            this.HP = hp;
            this.Offense = offense;
            this.Defense = defense;
            this.TroopCount = troopCount;
        }
    }
}
