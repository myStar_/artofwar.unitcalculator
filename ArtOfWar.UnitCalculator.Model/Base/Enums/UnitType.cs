﻿using System.ComponentModel;

namespace ArtOfWar.UnitCalculator.Model.Base.Enums
{
    public enum UnitType
    {
        empty,

        // T1
        [Description("Infanterie")]
        T1_Infanterie =1,
        [Description("Bogenschützen")]
        T1_Bogenschuetzen =2,
        [Description("Eiserne Wachen")]
        T1_EiserneWachen =3,
        [Description("Bomber")]
        T1_Bomber,
        [Description("Katapult")]
        T1_Katapult,
        [Description("Höllenjäger")]
        T1_Hoellenjaeger,

        // T2
        [Description("Feuermagier")]
        T2_Feuermagier,
        [Description("Banditen")]
        T2_Banditen,
        [Description("Ogerkrieger")]
        T2_Ogerkrieger,
        [Description("Assassinengeister")]
        T2_Assassinengeister,
        [Description("Zauberlehrling")]
        T2_Zauberlehrling, 
        [Description("Wikinger Krieger")]
        T2_WikingerKrieger,
        [Description("Eismagier")]
        T2_Eismagier,
        [Description("Der Gelehrte")]
        T2_DerGelehrte,
        [Description("Inquisitor")]
        T2_Inquisitor,
        [Description("Untoter Soldat")]
        T2_UntoterSoldat,
        [Description("Vorbote des Feuers")]
        T2_VorboteDesFeuers,
        [Description("Paladin")]
        T2_Paladin,
        [Description("Ballista")]
        T2_Ballista,
        [Description("Goblikazes")]
        T2_Goblikazes,
        [Description("Kakteen")]
        T2_Kakteen,

        // T3
        [Description("Nekromant")]
        T3_Nekromant,
        [Description("Pilger")]
        T3_Pilger,
        [Description("Yasha")]
        T3_Yasha,
        [Description("Heiliger Magier")]
        T3_HeiligerMagier,
        [Description("Seelenjäger")]
        T3_Seelenjaeger,
        [Description("Tempelritter")]
        T3_Tempelritter,
        [Description("Peltats")]
        T3_Peltats,
        [Description("Schläger")]
        T3_Schlaeger,
        [Description("Tauren-Hexer")]
        T3_TaurenHexer,
        [Description("Vodoo-Puppen")]
        T3_VodooPuppen,

        // T4
        [Description("Dämon")]
        T4_Daemon,
        [Description("Tiermeister")]
        T4_Tiermeister,
        [Description("Totem der Hexerei")]
        T4_TotemDerHexerei,
        [Description("Meteor Golem")]
        T4_MeteorGolem,
        [Description("Frostbogenschützen")]
        T4_Frostbogenschuetzen,
        [Description("Heiliger Schwertkämpfer")]
        T4_HeiligerSchwertkaempfer, 
        [Description("Nashorn-Ritter")]
        T4_NashornRitter
    }
}
