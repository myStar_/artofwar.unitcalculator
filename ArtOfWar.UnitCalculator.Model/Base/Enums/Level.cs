﻿namespace ArtOfWar.UnitCalculator.Model.Base.Enums
{
    public enum Level
    {
        empty = 0,

        lvl_1 = 1,
        lvl_2 = 2,
        lvl_3 = 3,
        lvl_4 = 4,
        lvl_5 = 5,
        lvl_6 = 6,
        lvl_7 = 7,
        lvl_8 = 8,
        lvl_9 = 9,
        lvl_10 = 10,
    }
}
