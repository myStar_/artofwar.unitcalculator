﻿using System.ComponentModel;

namespace ArtOfWar.UnitCalculator.Model.Base.Enums
{
    public enum Tier
    {
        empty,

        [Description("Normal")]
        Normal = 1,
        [Description("Selten")]
        Seldom = 2,
        [Description("Magisch")]
        Epic = 3,
        [Description("Legendär")]
        Legendary = 4
    }
}
