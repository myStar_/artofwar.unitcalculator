﻿using System.ComponentModel;

namespace ArtOfWar.UnitCalculator.Model.Base.Enums
{
    public enum Skill
    {
        [Description("nein")]
        empty = 0,

        [Description("Fliegt hinter Gegner")]
        FlyBehindEnemy,
        [Description("Teleportiert hinter Gegner")]
        TeleportBehindEnemy,
        [Description("Betäubt Gegner")]
        Stun,
        [Description("Friert Gegner ein")]
        Freeze,
        [Description("Belebt sich 1x wieder")]
        ReviceOnce,
        [Description("Hat einen Begleiter")]
        HasCompanion,
        [Description("Erzeugt neue Einheiten")]
        SpawnsUnits,
        [Description("Explodiert bei Feindkontakt")]
        Explodes,
        [Description("Schleudert Gegner weg")]
        KnockBack,
        [Description("Mehr Schaden bei wenig Leben")]
        MoreDamageOnLowHP,
        [Description("Hat mehrere Attacken")]
        MultipleAttacks,
        [Description("Wirft Schwerter um sich")]
        ThrowsSwords,
        [Description("Attackiert währrend Bewegung")]
        AttacksWhileMoving,
        [Description("Hat eine Spezial Schwertattacke")]
        SwordAttack,
        [Description("Rollt einen Meteor durch Gegner")]
        RollsStone,
        [Description("Rennt durch Gegner")]
        RidesThroughEnemys,
    }
}
