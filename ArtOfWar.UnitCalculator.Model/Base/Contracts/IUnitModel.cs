﻿using System.Collections.Generic;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Base.Contracts
{
    public interface IUnitModel
    {
        Tier Tier { get; set; }
        UnitType UnitType { get; set; }
        Skill Skill { get; set; }
        double Attackspeed { get; set; }
        bool? HasAreaOfEffect { get; set; }
        List<UnitLevelModel> UnitLevels { get; set; }
    }
}