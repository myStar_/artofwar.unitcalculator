﻿using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Base.Contracts
{
    public interface IUnitLevelModel
    {
        Level Level { get; }
        int HP { get; set; }
        int Offense { get; set; }
        int Defense { get; set; }
        int TroopCount { get; set; }
        double DPS { get; }
    }
}