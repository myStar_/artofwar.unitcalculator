﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T3
{
    public class HeiligerMagier : UnitModel
    {
        public HeiligerMagier()
        {
            this.Tier = Tier.Epic;

            this.UnitType = UnitType.T3_HeiligerMagier;

            this.Attackspeed = 0.7;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     700,    120,   35,     1),
                new UnitLevelModel(this,Level.lvl_2,     1200,   160,   40,     1),
                new UnitLevelModel(this,Level.lvl_3,     2200,   200,   45,     1),
                new UnitLevelModel(this,Level.lvl_4,     3500,   260,   55,     1),
                new UnitLevelModel(this,Level.lvl_5,     4800,   320,   60,     1),
                new UnitLevelModel(this,Level.lvl_6,     6200,   380,   65,     1),
                new UnitLevelModel(this,Level.lvl_7,     7600,   440,   70,     1),
                new UnitLevelModel(this,Level.lvl_8,     9000,   500,   75,     1),
                new UnitLevelModel(this,Level.lvl_9,     15750,  875,   80,     1),
                new UnitLevelModel(this,Level.lvl_10,    22500, 1250,   85,     1)
            });
        }
    }
}
