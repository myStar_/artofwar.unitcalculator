﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T3
{
    public class Pilger : UnitModel
    {
        public Pilger()
        {
            this.Tier = Tier.Epic;

            this.UnitType = UnitType.T3_Pilger;

            this.Attackspeed = 1.0;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     1200,   100,    44,     3),
                new UnitLevelModel(this,Level.lvl_2,     1500,   120,    50,     5),
                new UnitLevelModel(this,Level.lvl_3,     2200,   160,    56,     7),
                new UnitLevelModel(this,Level.lvl_4,     3500,   220,    69,     9),
                new UnitLevelModel(this,Level.lvl_5,     5400,   260,    75,     9),
                new UnitLevelModel(this,Level.lvl_6,     7300,   300,    81,     9),
                new UnitLevelModel(this,Level.lvl_7,     9200,   340,    88,     9),
                new UnitLevelModel(this,Level.lvl_8,     11100,  380,    94,     9),
                new UnitLevelModel(this,Level.lvl_9,     19425,  665,    169,    9),
                new UnitLevelModel(this,Level.lvl_10,    27750,  950,    238,    9)
            });
        }
    }
}
