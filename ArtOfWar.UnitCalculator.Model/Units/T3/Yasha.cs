﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T3
{
    public class Yasha : UnitModel
    {
        public Yasha()
        {
            this.Tier = Tier.Epic;

            this.UnitType = UnitType.T3_Yasha;

            this.Attackspeed = 1.1;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     2000,  180,    30,     1),
                new UnitLevelModel(this,Level.lvl_2,     4000,  220,    35,     1),
                new UnitLevelModel(this,Level.lvl_3,     6000,  260,    40,     1),
                new UnitLevelModel(this,Level.lvl_4,     8000,  300,    45,     1),
                new UnitLevelModel(this,Level.lvl_5,     10000, 340,    50,     1),
                new UnitLevelModel(this,Level.lvl_6,     13000, 380,    55,     1),
                new UnitLevelModel(this,Level.lvl_7,     17000, 420,    60,     1),
                new UnitLevelModel(this,Level.lvl_8,     22000, 460,    65,     1),
                new UnitLevelModel(this,Level.lvl_9,     38500, 805,    70,     1),
                new UnitLevelModel(this,Level.lvl_10,    55000, 1150,   75,     1)
            });
        }
    }
}
