﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T3
{
    public class Peltats : UnitModel
    {
        public Peltats()
        {
            this.Tier = Tier.Epic;

            this.UnitType = UnitType.T3_Peltats;

            this.Attackspeed = 0.9;

            this.Skill = Skill.AttacksWhileMoving;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     1000,   140,   65,      3),
                new UnitLevelModel(this,Level.lvl_2,     1250,   160,   70,      5),
                new UnitLevelModel(this,Level.lvl_3,     1750,   210,   75,      7),
                new UnitLevelModel(this,Level.lvl_4,     2800,   280,   80,      9),
                new UnitLevelModel(this,Level.lvl_5,     3750,   320,   85,      9),
                new UnitLevelModel(this,Level.lvl_6,     5000,   370,   90,      9),
                new UnitLevelModel(this,Level.lvl_7,     7250,   410,   95,      9),
                new UnitLevelModel(this,Level.lvl_8,     9000,   440,   100,     9),
                new UnitLevelModel(this,Level.lvl_9,     15750,  770,   105,     9),
                new UnitLevelModel(this,Level.lvl_10,    22500, 1100,   110,     9)
            });
        }
    }
}
