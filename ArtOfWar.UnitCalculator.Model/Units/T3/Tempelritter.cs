﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T3
{
    public class Tempelritter : UnitModel
    {
        public Tempelritter()
        {
            this.Tier = Tier.Epic;

            this.UnitType = UnitType.T3_Tempelritter;

            this.Attackspeed = 1.1;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     3000,  200,    45,     1),
                new UnitLevelModel(this,Level.lvl_2,     5000,  240,    50,     1),
                new UnitLevelModel(this,Level.lvl_3,     8000,  280,    55,     1),
                new UnitLevelModel(this,Level.lvl_4,     12500, 320,    60,     1),
                new UnitLevelModel(this,Level.lvl_5,     17000, 360,    65,     1),
                new UnitLevelModel(this,Level.lvl_6,     21500, 400,    70,     1),
                new UnitLevelModel(this,Level.lvl_7,     26000, 440,    75,     1),
                new UnitLevelModel(this,Level.lvl_8,     30500, 480,    80,     1),
                new UnitLevelModel(this,Level.lvl_9,     35000, 520,    85,     1),
                new UnitLevelModel(this,Level.lvl_10,    39500, 560,    90,     1)
            });
        }
    }
}
