﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T3
{
    public class Nekromant : UnitModel
    {
        public Nekromant()
        {
            this.Tier = Tier.Epic;

            this.UnitType = UnitType.T3_Nekromant;

            this.Attackspeed = 0.35;

            this.Skill = Skill.SpawnsUnits;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     1000,  0,    25,     1),
                new UnitLevelModel(this,Level.lvl_2,     1800,  0,    30,     1),
                new UnitLevelModel(this,Level.lvl_3,     2600,  0,    35,     1),
                new UnitLevelModel(this,Level.lvl_4,     3800,  0,    40,     1),
                new UnitLevelModel(this,Level.lvl_5,     5000,  0,    45,     1),
                new UnitLevelModel(this,Level.lvl_6,     6200,  0,    50,     1),
                new UnitLevelModel(this,Level.lvl_7,     7400,  0,    55,     1),
                new UnitLevelModel(this,Level.lvl_8,     8600,  0,    60,     1),
                new UnitLevelModel(this,Level.lvl_9,     15050, 0,    65,     1),
                new UnitLevelModel(this,Level.lvl_10,    21500, 0,    70,     1)
            });
        }
    }
}
