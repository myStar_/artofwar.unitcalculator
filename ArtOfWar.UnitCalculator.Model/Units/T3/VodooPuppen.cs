﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T3
{
    public class VodooPuppen : UnitModel
    {
        public VodooPuppen()
        {
            this.Tier = Tier.Epic;

            this.UnitType = UnitType.T3_VodooPuppen;

            this.Attackspeed = 1.3;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     600,    250,    0,    3),
                new UnitLevelModel(this,Level.lvl_2,     700,    290,    0,    5),
                new UnitLevelModel(this,Level.lvl_3,     800,    340,    0,    7),
                new UnitLevelModel(this,Level.lvl_4,     1100,   400,    0,    9),
                new UnitLevelModel(this,Level.lvl_5,     1300,   470,    0,    9),
                new UnitLevelModel(this,Level.lvl_6,     1600,   550,    0,    9),
                new UnitLevelModel(this,Level.lvl_7,     2000,   640,    0,    9),
                new UnitLevelModel(this,Level.lvl_8,     2500,   740,    0,    9),
                new UnitLevelModel(this,Level.lvl_9,     4375,  1295,    0,    9),
                new UnitLevelModel(this,Level.lvl_10,    6250,  1850,    0,    9)
            });
        }
    }
}
