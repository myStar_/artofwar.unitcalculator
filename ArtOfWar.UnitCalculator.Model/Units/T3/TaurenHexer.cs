﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T3
{
    public class TaurenHexer : UnitModel
    {
        public TaurenHexer()
        {
            this.Tier = Tier.Epic;

            this.UnitType = UnitType.T3_TaurenHexer;

            this.Attackspeed = 0.9;

            this.Skill = Skill.MultipleAttacks;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     600,    110,   20,     3),
                new UnitLevelModel(this,Level.lvl_2,     700,    130,   25,     5),
                new UnitLevelModel(this,Level.lvl_3,     950,    175,   30,     7),
                new UnitLevelModel(this,Level.lvl_4,     1500,   225,   35,     9),
                new UnitLevelModel(this,Level.lvl_5,     2800,   270,   40,     9),
                new UnitLevelModel(this,Level.lvl_6,     4100,   330,   45,     9),
                new UnitLevelModel(this,Level.lvl_7,     5400,   390,   50,     9),
                new UnitLevelModel(this,Level.lvl_8,     6800,   450,   55,     9),
                new UnitLevelModel(this,Level.lvl_9,     11900,  788,   60,     9),
                new UnitLevelModel(this,Level.lvl_10,    17000, 1125,   65,     9)
            });
        }
    }
}
