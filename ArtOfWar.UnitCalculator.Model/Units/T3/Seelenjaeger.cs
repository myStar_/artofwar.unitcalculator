﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T3
{
    public class Seelenjaeger : UnitModel
    {
        public Seelenjaeger()
        {
            this.Tier = Tier.Epic;

            this.UnitType = UnitType.T3_Seelenjaeger;

            this.Attackspeed = 1.1;

            this.Skill = Skill.ThrowsSwords;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     2000,   200,   30,     1),
                new UnitLevelModel(this,Level.lvl_2,     4000,   240,   35,     1),
                new UnitLevelModel(this,Level.lvl_3,     7500,   280,   40,     1),
                new UnitLevelModel(this,Level.lvl_4,     12000,  320,   45,     1),
                new UnitLevelModel(this,Level.lvl_5,     16500,  360,   50,     1),
                new UnitLevelModel(this,Level.lvl_6,     21000,  400,   55,     1),
                new UnitLevelModel(this,Level.lvl_7,     25500,  440,   60,     1),
                new UnitLevelModel(this,Level.lvl_8,     30000,  480,   65,     1),
                new UnitLevelModel(this,Level.lvl_9,     52500,  840,   70,     1),
                new UnitLevelModel(this,Level.lvl_10,    75000, 1200,   75,     1)
            });
        }
    }
}
