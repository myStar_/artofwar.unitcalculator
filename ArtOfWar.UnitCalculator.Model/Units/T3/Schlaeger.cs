﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T3
{
    public class Schlaeger : UnitModel
    {
        public Schlaeger()
        {
            this.Tier = Tier.Epic;

            this.UnitType = UnitType.T3_Schlaeger;

            this.Attackspeed = 1.0;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     900,    180,   40,     3),
                new UnitLevelModel(this,Level.lvl_2,     1000,   200,   45,     5),
                new UnitLevelModel(this,Level.lvl_3,     1300,   230,   50,     7),
                new UnitLevelModel(this,Level.lvl_4,     1900,   260,   55,     9),
                new UnitLevelModel(this,Level.lvl_5,     3400,   290,   60,     9),
                new UnitLevelModel(this,Level.lvl_6,     4800,   320,   65,     9),
                new UnitLevelModel(this,Level.lvl_7,     6200,   360,   70,     9),
                new UnitLevelModel(this,Level.lvl_8,     7600,   400,   75,     9),
                new UnitLevelModel(this,Level.lvl_9,     13300,  700,   80,     9),
                new UnitLevelModel(this,Level.lvl_10,    19000, 1000,   85,     9)
            });
        }
    }
}
