﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class Banditen : UnitModel
    {
        public Banditen()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_Banditen;

            this.Attackspeed = 1.25;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     800,    120,    20,     3),
                new UnitLevelModel(this,Level.lvl_2,     900,    140,    25,     5),
                new UnitLevelModel(this,Level.lvl_3,     1200,   180,    30,     7),
                new UnitLevelModel(this,Level.lvl_4,     1800,   240,    35,     9),
                new UnitLevelModel(this,Level.lvl_5,     3200,   280,    40,     9),
                new UnitLevelModel(this,Level.lvl_6,     4600,   340,    45,     9),
                new UnitLevelModel(this,Level.lvl_7,     6000,   400,    50,     9),
                new UnitLevelModel(this,Level.lvl_8,     7400,   440,    55,     9),
                new UnitLevelModel(this,Level.lvl_9,     12950,  770,    60,     9),
                new UnitLevelModel(this,Level.lvl_10,    18500,  1100,   65,     9)
            });
        }
    }
}
