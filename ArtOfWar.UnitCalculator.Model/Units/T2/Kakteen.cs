﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class Kakteen : UnitModel
    {
        public Kakteen()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_Kakteen;

            this.Attackspeed = 0.9;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     900,    150,    20,     3),
                new UnitLevelModel(this,Level.lvl_2,     1100,   180,    25,     5),
                new UnitLevelModel(this,Level.lvl_3,     1500,   220,    30,     7),
                new UnitLevelModel(this,Level.lvl_4,     2000,   270,    35,     9),
                new UnitLevelModel(this,Level.lvl_5,     3400,   330,    40,     9),
                new UnitLevelModel(this,Level.lvl_6,     4800,   370,    45,     9),
                new UnitLevelModel(this,Level.lvl_7,     6300,   420,    50,     9),
                new UnitLevelModel(this,Level.lvl_8,     7700,   460,    55,     9),
                new UnitLevelModel(this,Level.lvl_9,     13475,  805,    60,     9),
                new UnitLevelModel(this,Level.lvl_10,    19250,  1150,   65,     9)
            });
        }
    }
}
