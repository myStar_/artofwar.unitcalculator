﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class Goblikazes : UnitModel
    {
        public Goblikazes()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_Goblikazes;

            this.Attackspeed = 0.0;

            this.Skill = Skill.Explodes;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     450,    0,     5,     3),
                new UnitLevelModel(this,Level.lvl_2,     900,    0,    10,     5),
                new UnitLevelModel(this,Level.lvl_3,     1200,   0,    15,     7),
                new UnitLevelModel(this,Level.lvl_4,     1800,   0,    20,     9),
                new UnitLevelModel(this,Level.lvl_5,     2400,   0,    25,     9),
                new UnitLevelModel(this,Level.lvl_6,     3800,   0,    30,     9),
                new UnitLevelModel(this,Level.lvl_7,     4700,   0,    35,     9),
                new UnitLevelModel(this,Level.lvl_8,     5500,   0,    40,     9),
                new UnitLevelModel(this,Level.lvl_9,     9625,   0,    45,     9),
                new UnitLevelModel(this,Level.lvl_10,    13750,  0,    50,     9)
            });
        }
    }
}
