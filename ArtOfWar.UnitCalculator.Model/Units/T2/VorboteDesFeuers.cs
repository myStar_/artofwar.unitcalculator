﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class VorboteDesFeuers : UnitModel
    {
        public VorboteDesFeuers()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_VorboteDesFeuers;

            this.Attackspeed = 0.8;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     1000,   100,    25,     3),
                new UnitLevelModel(this,Level.lvl_2,     1250,   120,    30,     5),
                new UnitLevelModel(this,Level.lvl_3,     1750,   160,    35,     7),
                new UnitLevelModel(this,Level.lvl_4,     2800,   220,    40,     9),
                new UnitLevelModel(this,Level.lvl_5,     3750,   280,    45,     9),
                new UnitLevelModel(this,Level.lvl_6,     5000,   350,    50,     9),
                new UnitLevelModel(this,Level.lvl_7,     7250,   420,    55,     9),
                new UnitLevelModel(this,Level.lvl_8,     9000,   500,    60,     9),
                new UnitLevelModel(this,Level.lvl_9,     15750,  875,    65,     9),
                new UnitLevelModel(this,Level.lvl_10,    22500,  1250,   70,     9)
            });
        }
    }
}
