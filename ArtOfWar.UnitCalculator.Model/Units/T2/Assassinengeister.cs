﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class Assassinengeister : UnitModel
    {
        public Assassinengeister()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_Assassinengeister;

            this.Attackspeed = 1.2;

            this.Skill = Skill.TeleportBehindEnemy;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     700,    110,    10,     3),
                new UnitLevelModel(this,Level.lvl_2,     800,    130,    15,     5),
                new UnitLevelModel(this,Level.lvl_3,     1000,   170,    20,     7),
                new UnitLevelModel(this,Level.lvl_4,     1500,   230,    25,     9),
                new UnitLevelModel(this,Level.lvl_5,     2800,   270,    30,     9),
                new UnitLevelModel(this,Level.lvl_6,     4000,   330,    35,     9),
                new UnitLevelModel(this,Level.lvl_7,     5500,   390,    40,     9),
                new UnitLevelModel(this,Level.lvl_8,     7000,   450,    45,     9),
                new UnitLevelModel(this,Level.lvl_9,     12250,  788,    50,     9),
                new UnitLevelModel(this,Level.lvl_10,    17500,  1125,   55,     9)
            });
        }
    }
}
