﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class Paladin : UnitModel
    {
        public Paladin()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_Paladin;

            this.Attackspeed = 1.0;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     1200,   100,    60,     3),
                new UnitLevelModel(this,Level.lvl_2,     1500,   140,    65,     5),
                new UnitLevelModel(this,Level.lvl_3,     2200,   200,    70,     7),
                new UnitLevelModel(this,Level.lvl_4,     3500,   240,    75,     9),
                new UnitLevelModel(this,Level.lvl_5,     5400,   280,    80,     9),
                new UnitLevelModel(this,Level.lvl_6,     7300,   320,    85,     9),
                new UnitLevelModel(this,Level.lvl_7,     9200,   360,    90,     9),
                new UnitLevelModel(this,Level.lvl_8,     11100,  400,    95,     9),
                new UnitLevelModel(this,Level.lvl_9,     19425,  700,    100,    9),
                new UnitLevelModel(this,Level.lvl_10,    27750,  1000,   105,    9)
            });
        }
    }
}
