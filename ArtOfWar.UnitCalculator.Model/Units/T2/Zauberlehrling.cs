﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class Zauberlehrling : UnitModel
    {
        public Zauberlehrling()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_Zauberlehrling;

            this.Attackspeed = 0.8;

            this.Skill = Skill.FlyBehindEnemy;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     500,     80,     5,     3),
                new UnitLevelModel(this,Level.lvl_2,     800,    100,    10,     5),
                new UnitLevelModel(this,Level.lvl_3,     1200,   140,    15,     7),
                new UnitLevelModel(this,Level.lvl_4,     1800,   200,    20,     9),
                new UnitLevelModel(this,Level.lvl_5,     2400,   240,    25,     9),
                new UnitLevelModel(this,Level.lvl_6,     3000,   300,    30,     9),
                new UnitLevelModel(this,Level.lvl_7,     3600,   340,    35,     9),
                new UnitLevelModel(this,Level.lvl_8,     4200,   380,    40,     9),
                new UnitLevelModel(this,Level.lvl_9,     7350,   665,    45,     9),
                new UnitLevelModel(this,Level.lvl_10,    10500,  950,    50,     9)
            });
        }
    }
}
