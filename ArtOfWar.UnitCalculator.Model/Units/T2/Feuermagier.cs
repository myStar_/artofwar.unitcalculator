﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class Feuermagier : UnitModel
    {
        public Feuermagier()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_Feuermagier;

            this.Attackspeed = 0.7;

            this.HasAreaOfEffect = true;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     600,    100,    20,     1),
                new UnitLevelModel(this,Level.lvl_2,     1200,   120,    25,     1),
                new UnitLevelModel(this,Level.lvl_3,     2000,   160,    30,     1),
                new UnitLevelModel(this,Level.lvl_4,     3200,   220,    35,     1),
                new UnitLevelModel(this,Level.lvl_5,     4400,   280,    40,     1),
                new UnitLevelModel(this,Level.lvl_6,     5600,   350,    45,     1),
                new UnitLevelModel(this,Level.lvl_7,     6800,   420,    50,     1),
                new UnitLevelModel(this,Level.lvl_8,     8000,   500,    55,     1),
                new UnitLevelModel(this,Level.lvl_9,     14000,  875,    60,     1),
                new UnitLevelModel(this,Level.lvl_10,    20000,  1250,   65,     1)
            });
        }
    }
}
