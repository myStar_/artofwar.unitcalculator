﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class DerGelehrte : UnitModel
    {
        public DerGelehrte()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_DerGelehrte;

            this.Attackspeed = 0.7;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     600,    100,    20,     1),
                new UnitLevelModel(this,Level.lvl_2,     1100,   120,    25,     1),
                new UnitLevelModel(this,Level.lvl_3,     2000,   140,    30,     1),
                new UnitLevelModel(this,Level.lvl_4,     3000,   200,    35,     1),
                new UnitLevelModel(this,Level.lvl_5,     4250,   240,    40,     1),
                new UnitLevelModel(this,Level.lvl_6,     5500,   300,    45,     1),
                new UnitLevelModel(this,Level.lvl_7,     6750,   360,    50,     1),
                new UnitLevelModel(this,Level.lvl_8,     8000,   420,    55,     1),
                new UnitLevelModel(this,Level.lvl_9,     14000,  735,    60,     1),
                new UnitLevelModel(this,Level.lvl_10,    20000,  1050,   65,     1)
            });
        }
    }
}
