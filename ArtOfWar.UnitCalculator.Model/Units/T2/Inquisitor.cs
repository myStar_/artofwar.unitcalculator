﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class Inquisitor : UnitModel
    {
        public Inquisitor()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_Inquisitor;

            this.Attackspeed = 1.2;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     900,    150,    45,     3),
                new UnitLevelModel(this,Level.lvl_2,     1000,   175,    50,     5),
                new UnitLevelModel(this,Level.lvl_3,     1300,   225,    55,     7),
                new UnitLevelModel(this,Level.lvl_4,     1900,   275,    60,     9),
                new UnitLevelModel(this,Level.lvl_5,     3300,   325,    65,     9),
                new UnitLevelModel(this,Level.lvl_6,     4700,   375,    70,     9),
                new UnitLevelModel(this,Level.lvl_7,     6100,   425,    75,     9),
                new UnitLevelModel(this,Level.lvl_8,     7500,   475,    80,     9),
                new UnitLevelModel(this,Level.lvl_9,     13125,  831,    85,     9),
                new UnitLevelModel(this,Level.lvl_10,    18750,  1188,   90,     9)
            });
        }
    }
}
