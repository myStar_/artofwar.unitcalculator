﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class Ogerkrieger : UnitModel
    {
        public Ogerkrieger()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_Ogerkrieger;

            this.Attackspeed = 0.9;

            this.Skill = Skill.KnockBack;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     2000,   200,    30,     1),
                new UnitLevelModel(this,Level.lvl_2,     4000,   220,    35,     1),
                new UnitLevelModel(this,Level.lvl_3,     6000,   240,    40,     1),
                new UnitLevelModel(this,Level.lvl_4,     8000,   260,    45,     1),
                new UnitLevelModel(this,Level.lvl_5,     10000,  280,    50,     1),
                new UnitLevelModel(this,Level.lvl_6,     13000,  310,    55,     1),
                new UnitLevelModel(this,Level.lvl_7,     16000,  350,    60,     1),
                new UnitLevelModel(this,Level.lvl_8,     20000,  390,    65,     1),
                new UnitLevelModel(this,Level.lvl_9,     35000,  682,    70,     1),
                new UnitLevelModel(this,Level.lvl_10,    50000,  975,    75,     1)
            });
        }
    }
}
