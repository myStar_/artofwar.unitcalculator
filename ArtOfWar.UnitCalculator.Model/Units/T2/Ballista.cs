﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class Ballista : UnitModel
    {
        public Ballista()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_Ballista;

            this.Attackspeed = 1.0;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     1100,   100,    15,     1),
                new UnitLevelModel(this,Level.lvl_2,     1900,   140,    20,     1),
                new UnitLevelModel(this,Level.lvl_3,     2500,   200,    25,     1),
                new UnitLevelModel(this,Level.lvl_4,     3300,   240,    30,     1),
                new UnitLevelModel(this,Level.lvl_5,     4200,   280,    35,     1),
                new UnitLevelModel(this,Level.lvl_6,     5100,   320,    40,     1),
                new UnitLevelModel(this,Level.lvl_7,     6200,   360,    45,     1),
                new UnitLevelModel(this,Level.lvl_8,     7200,   400,    50,     1),
                new UnitLevelModel(this,Level.lvl_9,     12600,  700,    55,     1),
                new UnitLevelModel(this,Level.lvl_10,    18000,  1000,   60,     1)
            });
        }
    }
}
