﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T2
{
    public class UntoterSoldat : UnitModel
    {
        public UntoterSoldat()
        {
            this.Tier = Tier.Seldom;

            this.UnitType = UnitType.T2_UntoterSoldat;

            this.Attackspeed = 1.3;

            this.Skill = Skill.ReviceOnce;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     800,    110,    25,     3),
                new UnitLevelModel(this,Level.lvl_2,     900,    130,    30,     5),
                new UnitLevelModel(this,Level.lvl_3,     1200,   170,    35,     7),
                new UnitLevelModel(this,Level.lvl_4,     1800,   230,    40,     9),
                new UnitLevelModel(this,Level.lvl_5,     3200,   270,    45,     9),
                new UnitLevelModel(this,Level.lvl_6,     4600,   320,    50,     9),
                new UnitLevelModel(this,Level.lvl_7,     6000,   360,    55,     9),
                new UnitLevelModel(this,Level.lvl_8,     7400,   400,    60,     9),
                new UnitLevelModel(this,Level.lvl_9,     12950,  700,    65,     9),
                new UnitLevelModel(this,Level.lvl_10,    18500,  1000,   70,     9)
            });
        }
    }
}
