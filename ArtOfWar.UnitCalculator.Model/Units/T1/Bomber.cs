﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T1
{
    public class Bomber :UnitModel
    {
        public Bomber()
        {
            this.Tier = Tier.Normal;

            this.UnitType = UnitType.T1_Bomber;

            this.HasAreaOfEffect = true;

            this.Attackspeed = 0.7;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     500,    80,     10,     3),
                new UnitLevelModel(this,Level.lvl_2,     1000,   100,    15,     5),
                new UnitLevelModel(this,Level.lvl_3,     1600,   140,    20,     7),
                new UnitLevelModel(this,Level.lvl_4,     2400,   200,    25,     9),
                new UnitLevelModel(this,Level.lvl_5,     3200,   240,    30,     9),
                new UnitLevelModel(this,Level.lvl_6,     4200,   300,    35,     9),
                new UnitLevelModel(this,Level.lvl_7,     5200,   340,    40,     9),
                new UnitLevelModel(this,Level.lvl_8,     6200,   400,    45,     9),
                new UnitLevelModel(this,Level.lvl_9,     10850,  700,    50,     9),
                new UnitLevelModel(this,Level.lvl_10,    15500,  1000,   55,     9)
            });
        }
    }
}
