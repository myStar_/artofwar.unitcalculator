﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T1
{
    public class EiserneWachen : UnitModel
    {
        public EiserneWachen()
        {
            this.Tier = Tier.Normal;

            this.UnitType = UnitType.T1_EiserneWachen;

            this.Attackspeed = 1.1;

            this.UnitLevels.AddRange(new[]
{
                new UnitLevelModel(this,Level.lvl_1,     1000,    80,    35,     3),
                new UnitLevelModel(this,Level.lvl_2,     1200,   100,    40,     5),
                new UnitLevelModel(this,Level.lvl_3,     1500,   140,    45,     7),
                new UnitLevelModel(this,Level.lvl_4,     2200,   200,    55,     9),
                new UnitLevelModel(this,Level.lvl_5,     3800,   240,    60,     9),
                new UnitLevelModel(this,Level.lvl_6,     5200,   280,    65,     9),
                new UnitLevelModel(this,Level.lvl_7,     6600,   320,    70,     9),
                new UnitLevelModel(this,Level.lvl_8,     8000,   360,    75,     9),
                new UnitLevelModel(this,Level.lvl_9,     14000,  630,    80,     9),
                new UnitLevelModel(this,Level.lvl_10,    20000,  900,    85,     9)
            });
        }
    }
}
