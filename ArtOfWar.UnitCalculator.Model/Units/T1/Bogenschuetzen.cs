﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T1
{
    public class Bogenschuetzen : UnitModel
    {
        public Bogenschuetzen()
        {
            this.Tier = Tier.Normal;

            this.UnitType = UnitType.T1_Bogenschuetzen;

            this.Attackspeed = 0.8;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this, Level.lvl_1,     600,    20,     20,     3),
                new UnitLevelModel(this,Level.lvl_2,     700,    40,     25,     5),
                new UnitLevelModel(this,Level.lvl_3,     950,    180,    30,     7),
                new UnitLevelModel(this,Level.lvl_4,     1500,   240,    35,     9),
                new UnitLevelModel(this,Level.lvl_5,     2800,   280,    40,     9),
                new UnitLevelModel(this,Level.lvl_6,     4100,   340,    45,     9),
                new UnitLevelModel(this,Level.lvl_7,     5400,   400,    50,     9),
                new UnitLevelModel(this,Level.lvl_8,     6800,   460,    60,     9),
                new UnitLevelModel(this,Level.lvl_9,     11900,  805,    65,     9),
                new UnitLevelModel(this,Level.lvl_10,    17000,  1150,   70,     9)
            });
        }
    }
}
