﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T1
{
    public class Katapult : UnitModel
    {
        public Katapult()
        {
            this.Tier = Tier.Normal;

            this.UnitType = UnitType.T1_Katapult;

            this.HasAreaOfEffect = true;

            this.Attackspeed = 0.2;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     1000,   200,    15,     1),
                new UnitLevelModel(this,Level.lvl_2,     1800,   240,    20,     1),
                new UnitLevelModel(this,Level.lvl_3,     2400,   280,    25,     1),
                new UnitLevelModel(this,Level.lvl_4,     3200,   320,    30,     1),
                new UnitLevelModel(this,Level.lvl_5,     4000,   360,    35,     1),
                new UnitLevelModel(this,Level.lvl_6,     5000,   400,    40,     1),
                new UnitLevelModel(this,Level.lvl_7,     6000,   440,    45,     1),
                new UnitLevelModel(this,Level.lvl_8,     7000,   480,    50,     1),
                new UnitLevelModel(this,Level.lvl_9,     12250,  840,    55,     1),
                new UnitLevelModel(this,Level.lvl_10,    17500,  1200,   60,     1)
            });
        }
    }
}
