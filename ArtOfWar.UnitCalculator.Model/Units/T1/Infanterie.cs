﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T1
{
    public class Infanterie : UnitModel
    {
        public Infanterie()
        {
            this.Tier = Tier.Normal;

            this.UnitType = UnitType.T1_Infanterie;

            this.Attackspeed = 1.1;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     800,    100,    30,     3),
                new UnitLevelModel(this,Level.lvl_2,     900,    120,    35,     5),
                new UnitLevelModel(this,Level.lvl_3,     1200,   160,    40,     7),
                new UnitLevelModel(this,Level.lvl_4,     1800,   220,    45,     9),
                new UnitLevelModel(this,Level.lvl_5,     3200,   260,    50,     9),
                new UnitLevelModel(this,Level.lvl_6,     4600,   300,    55,     9),
                new UnitLevelModel(this,Level.lvl_7,     6000,   340,    60,     9),
                new UnitLevelModel(this,Level.lvl_8,     7400,   380,    65,     9),
                new UnitLevelModel(this,Level.lvl_9,     12950,  665,    70,     9),
                new UnitLevelModel(this,Level.lvl_10,    18500,  950,    75,     9)
            });
        }
    }
}
