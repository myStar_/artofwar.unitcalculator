﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T1
{
    public class Hoellenjaeger : UnitModel
    {
        public Hoellenjaeger()
        {
            this.Tier = Tier.Normal;

            this.UnitType = UnitType.T1_Hoellenjaeger;

            this.Attackspeed = 1.1;

            this.Skill = Skill.FlyBehindEnemy;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     800,    110,    30,     3),
                new UnitLevelModel(this,Level.lvl_2,     900,    130,    35,     5),
                new UnitLevelModel(this,Level.lvl_3,     1200,   170,    40,     7),
                new UnitLevelModel(this,Level.lvl_4,     1800,   230,    45,     9),
                new UnitLevelModel(this,Level.lvl_5,     3200,   270,    50,     9),
                new UnitLevelModel(this,Level.lvl_6,     4600,   320,    55,     9),
                new UnitLevelModel(this,Level.lvl_7,     6000,   360,    60,     9),
                new UnitLevelModel(this,Level.lvl_8,     7400,   400,    65,     9),
                new UnitLevelModel(this,Level.lvl_9,     12950,  700,    70,     9),
                new UnitLevelModel(this,Level.lvl_10,    18500,  1000,   75,     9)
            });
        }
    }
}
