﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T4
{
    public class NashornRitter : UnitModel
    {
        public NashornRitter()
        {
            this.Tier = Tier.Legendary;

            this.UnitType = UnitType.T4_NashornRitter;

            this.Attackspeed = 2.0;

            this.Skill = Skill.RidesThroughEnemys;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     2000,   400,    50,     1),
                new UnitLevelModel(this,Level.lvl_2,     4000,   480,    60,     1),
                new UnitLevelModel(this,Level.lvl_3,     7500,   560,    70,     1),
                new UnitLevelModel(this,Level.lvl_4,     12000,  640,    80,     1),
                new UnitLevelModel(this,Level.lvl_5,     16500,  720,    90,     1),
                new UnitLevelModel(this,Level.lvl_6,     21000,  800,   100,     1),
                new UnitLevelModel(this,Level.lvl_7,     25500,  880,   110,     1),
                new UnitLevelModel(this,Level.lvl_8,     30000,  960,   120,     1),
                new UnitLevelModel(this,Level.lvl_9,     52500, 1680,   130,     1),
                new UnitLevelModel(this,Level.lvl_10,    75000, 2400,   140,     1)
            });
        }
    }
}
