﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T4
{
    public class Frostbogenschuetzen : UnitModel
    {
        public Frostbogenschuetzen()
        {
            this.Tier = Tier.Legendary;

            this.UnitType = UnitType.T4_Frostbogenschuetzen;

            this.Attackspeed = 0.8;

            this.Skill = Skill.Freeze;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     800,    130,    35,     3),
                new UnitLevelModel(this,Level.lvl_2,     900,    150,    40,     5),
                new UnitLevelModel(this,Level.lvl_3,     1200,   200,    45,     7),
                new UnitLevelModel(this,Level.lvl_4,     1800,   240,    50,     9),
                new UnitLevelModel(this,Level.lvl_5,     3200,   300,    55,     9),
                new UnitLevelModel(this,Level.lvl_6,     4600,   360,    60,     9),
                new UnitLevelModel(this,Level.lvl_7,     6000,   420,    65,     9),
                new UnitLevelModel(this,Level.lvl_8,     7400,   480,    70,     9),
                new UnitLevelModel(this,Level.lvl_9,     12950,  840,    75,     9),
                new UnitLevelModel(this,Level.lvl_10,    18500, 1200,    80,     9)
            });
        }
    }
}
