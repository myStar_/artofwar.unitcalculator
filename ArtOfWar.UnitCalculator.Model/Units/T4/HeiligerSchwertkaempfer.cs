﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T4
{
    public class HeiligerSchwertkaempfer : UnitModel
    {
        public HeiligerSchwertkaempfer()
        {
            this.Tier = Tier.Legendary;

            this.UnitType = UnitType.T4_HeiligerSchwertkaempfer;

            this.Attackspeed = 1.3;

            this.Skill = Skill.SwordAttack;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     2000,   180,    35,     1),
                new UnitLevelModel(this,Level.lvl_2,     4000,   210,    40,     1),
                new UnitLevelModel(this,Level.lvl_3,     7000,   250,    45,     1),
                new UnitLevelModel(this,Level.lvl_4,     12000,  300,    50,     1),
                new UnitLevelModel(this,Level.lvl_5,     16000,  360,    55,     1),
                new UnitLevelModel(this,Level.lvl_6,     20000,  440,    60,     1),
                new UnitLevelModel(this,Level.lvl_7,     25000,  540,    65,     1),
                new UnitLevelModel(this,Level.lvl_8,     31000,  630,    70,     1),
                new UnitLevelModel(this,Level.lvl_9,     54250, 1103,    75,     1),
                new UnitLevelModel(this,Level.lvl_10,    77500, 1575,    80,     1)
            });
        }
    }
}
