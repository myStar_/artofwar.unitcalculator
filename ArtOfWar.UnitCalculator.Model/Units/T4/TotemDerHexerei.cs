﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T4
{
    public class TotemDerHexerei : UnitModel
    {
        public TotemDerHexerei()
        {
            this.Tier = Tier.Legendary;

            this.UnitType = UnitType.T4_TotemDerHexerei;

            this.Attackspeed = 1.2;

            this.Skill = Skill.Stun;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     2000,   200,    35,     1),
                new UnitLevelModel(this,Level.lvl_2,     4000,   225,    40,     1),
                new UnitLevelModel(this,Level.lvl_3,     7000,   250,    45,     1),
                new UnitLevelModel(this,Level.lvl_4,     12000,  275,    50,     1),
                new UnitLevelModel(this,Level.lvl_5,     16000,  300,    55,     1),
                new UnitLevelModel(this,Level.lvl_6,     20000,  330,    60,     1),
                new UnitLevelModel(this,Level.lvl_7,     25000,  360,    65,     1),
                new UnitLevelModel(this,Level.lvl_8,     31000,  400,    70,     1),
                new UnitLevelModel(this,Level.lvl_9,     54250,  700,    75,     1),
                new UnitLevelModel(this,Level.lvl_10,    77500, 1000,    80,     1)
            });                                        
        }
    }
}
