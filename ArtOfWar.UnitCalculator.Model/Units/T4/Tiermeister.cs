﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T4
{
    public class Tiermeister : UnitModel
    {
        public Tiermeister()
        {
            this.Tier = Tier.Legendary;

            this.UnitType = UnitType.T4_Tiermeister;

            this.Attackspeed = 1.1;

            this.Skill = Skill.HasCompanion;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     2000,   220,    30,     1),
                new UnitLevelModel(this,Level.lvl_2,     4000,   250,    35,     1),
                new UnitLevelModel(this,Level.lvl_3,     7000,   280,    40,     1),
                new UnitLevelModel(this,Level.lvl_4,     10000,  310,    45,     1),
                new UnitLevelModel(this,Level.lvl_5,     14000,  340,    50,     1),
                new UnitLevelModel(this,Level.lvl_6,     18000,  370,    55,     1),
                new UnitLevelModel(this,Level.lvl_7,     22000,  400,    60,     1),
                new UnitLevelModel(this,Level.lvl_8,     26000,  430,    65,     1),
                new UnitLevelModel(this,Level.lvl_9,     45500,  753,    70,     1),
                new UnitLevelModel(this,Level.lvl_10,    65000, 1075,    75,     1)
            });
        }
    }
}
