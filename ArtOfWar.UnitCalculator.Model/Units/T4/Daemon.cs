﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T4
{
    public class Daemon : UnitModel
    {
        public Daemon()
        {
            this.Tier = Tier.Legendary;

            this.UnitType = UnitType.T4_Daemon;

            this.Attackspeed = 1.0;

            this.Skill = Skill.FlyBehindEnemy;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     2000,   220,    35,     1),
                new UnitLevelModel(this,Level.lvl_2,     4000,   280,    40,     1),
                new UnitLevelModel(this,Level.lvl_3,     7500,   340,    45,     1),
                new UnitLevelModel(this,Level.lvl_4,     12000,  400,    50,     1),
                new UnitLevelModel(this,Level.lvl_5,     16500,  450,    55,     1),
                new UnitLevelModel(this,Level.lvl_6,     21000,  500,    60,     1),
                new UnitLevelModel(this,Level.lvl_7,     27500,  575,    65,     1),
                new UnitLevelModel(this,Level.lvl_8,     34000,  650,    70,     1),
                new UnitLevelModel(this,Level.lvl_9,     59500, 1138,    75,     1),
                new UnitLevelModel(this,Level.lvl_10,    85000, 1625,    80,     1)
            });
        }
    }
}
