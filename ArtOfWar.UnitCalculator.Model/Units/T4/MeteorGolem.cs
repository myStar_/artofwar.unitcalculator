﻿using ArtOfWar.UnitCalculator.Model.Base;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Model.Units.T4
{
    public class MeteorGolem : UnitModel
    {
        public MeteorGolem()
        {
            this.Tier = Tier.Legendary;

            this.UnitType = UnitType.T4_MeteorGolem;

            this.Attackspeed = 0.9;

            this.Skill = Skill.RollsStone;

            this.UnitLevels.AddRange(new[]
            {
                new UnitLevelModel(this,Level.lvl_1,     2000,   240,    40,     1),
                new UnitLevelModel(this,Level.lvl_2,     4000,   300,    45,     1),
                new UnitLevelModel(this,Level.lvl_3,     7500,   360,    50,     1),
                new UnitLevelModel(this,Level.lvl_4,     12000,  420,    55,     1),
                new UnitLevelModel(this,Level.lvl_5,     16500,  480,    60,     1),
                new UnitLevelModel(this,Level.lvl_6,     21000,  550,    65,     1),
                new UnitLevelModel(this,Level.lvl_7,     27500,  620,    70,     1),
                new UnitLevelModel(this,Level.lvl_8,     34000,  690,    80,     1),
                new UnitLevelModel(this,Level.lvl_9,     59500, 1208,    90,     1),
                new UnitLevelModel(this,Level.lvl_10,    85000, 1725,   100,     1)
            });                                                     
        }
    }
}
