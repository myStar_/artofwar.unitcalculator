﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using ArtOfWar.UnitCalculator.Logic.Service;
using ArtOfWar.UnitCalculator.Logic.Service.Contracts;
using ArtOfWar.UnitCalculator.Model.Base.Contracts;
using ArtOfWar.UnitCalculator.Model.Base.Enums;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;

namespace ArtOfWar.UnitCalculator.Feature
{
    internal class UnitDetailRectViewModel : ViewModelBase
    {
        private readonly IUnitService unitService;

        public ICommand ButtonMinusCommand => new RelayCommand(this.ExecButtonMinusCommand);
        public ICommand ButtonPlusCommand => new RelayCommand(this.ExecButtonPlusCommand);

        public UnitDetailRectViewModel()
            : this(new HardCodedUnitService())
        {
            if (!this.IsInDesignMode)
            {
                throw new Exception("Empty cTor only in DesignTime...");
            }
        }

        [PreferredConstructor]
        public UnitDetailRectViewModel(IUnitService unitService)
        {
            this.unitService = unitService;

            this.InitData();
            this.InitControlDefaults();
        }

        private void InitData()
        {
            this.LevelList = ((Level[])Enum.GetValues(typeof(Level))).Where(x => !x.Equals(Level.empty)).ToList();

            this.UnitList = new ObservableCollection<IUnitModel>(this.unitService.GetAllUnits().OrderBy(x => (int)x.UnitType));
        }

        private void InitControlDefaults()
        {
            //If Unit List is NULL => Exception of Death

            this.SelectedUnit = this.UnitList.First();
            this.SelectedLevel = this.LevelList.First();
        }

        private void ExecButtonMinusCommand()
        {
            var idx = this.LevelList.IndexOf(this.SelectedLevel);

            if (idx < 1)
            {
                return;
            }

            this.SelectedLevel = this.LevelList[idx - 1];
        }

        private void ExecButtonPlusCommand()
        {
            var idx = this.LevelList.IndexOf(this.SelectedLevel);

            if (idx >= this.LevelList.Count - 1)
            {
                return;
            }

            this.SelectedLevel = this.LevelList[idx + 1];
        }

        private IUnitModel selectedUnit;
        public IUnitModel SelectedUnit
        {
            get => this.selectedUnit;
            set
            {
                this.Set(ref this.selectedUnit, value);

                if (this.SelectedUnit == null)
                {
                    return;
                }

                this.SelectedUnitLevel = this.SelectedUnit.UnitLevels.FirstOrDefault(x => x.Level.Equals(this.SelectedLevel));
            }
        }

        private ObservableCollection<IUnitModel> unitList;
        public ObservableCollection<IUnitModel> UnitList
        {
            get => this.unitList;
            set => this.Set(ref this.unitList, value);
        }

        private Level selectedLevel;
        public Level SelectedLevel
        {
            get => this.selectedLevel;
            set
            {
                this.Set(ref this.selectedLevel, value);

                if (this.SelectedUnit == null)
                {
                    return;
                }

                this.SelectedUnitLevel = this.SelectedUnit.UnitLevels.FirstOrDefault(x => x.Level.Equals(this.SelectedLevel));
            }
        }

        public List<Level> LevelList { get; private set; }

        private IUnitLevelModel selectedUnitLevel;

        public IUnitLevelModel SelectedUnitLevel
        {
            get => this.selectedUnitLevel;
            set => this.Set(ref this.selectedUnitLevel, value);
        }
    }
}
