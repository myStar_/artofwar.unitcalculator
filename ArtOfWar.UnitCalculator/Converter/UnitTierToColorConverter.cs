﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Converter
{
    internal class UnitTierToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Tier enumValue)
            {
                return enumValue switch
                {
                    Tier.empty => new SolidColorBrush(Colors.White), //nn
                    Tier.Normal => new SolidColorBrush(Colors.Gray),
                    Tier.Seldom => new SolidColorBrush(Colors.DeepSkyBlue),
                    Tier.Epic => new SolidColorBrush(Colors.Purple),
                    Tier.Legendary => new SolidColorBrush(Colors.Gold),
                    _ => new SolidColorBrush(Colors.Black) //nn
                };
            }

            throw new ArgumentException(nameof(value));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
