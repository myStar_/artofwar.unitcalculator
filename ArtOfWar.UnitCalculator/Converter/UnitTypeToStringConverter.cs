﻿using System;
using System.Globalization;
using System.Windows.Data;
using ArtOfWar.UnitCalculator.Logic.Extensions;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Converter
{
    internal class UnitTypeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is UnitType unitTypeValue)
            {
                return unitTypeValue.GetDescriptionAttribute();
            }

            throw new ArgumentException(nameof(value));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
