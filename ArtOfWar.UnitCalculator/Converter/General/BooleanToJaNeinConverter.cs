﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ArtOfWar.UnitCalculator.Converter.General
{
    internal class NullableBooleanToJaNeinConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return "?";
            }

            if (value is bool boolValue)
            {
                return boolValue ? "ja" : "nein";
            }

            throw new ArgumentException(nameof(value));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
