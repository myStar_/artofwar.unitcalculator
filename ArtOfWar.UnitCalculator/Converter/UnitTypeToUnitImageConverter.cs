﻿using System;
using System.Globalization;
using System.Reflection;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using ArtOfWar.UnitCalculator.Logic.Helper;
using ArtOfWar.UnitCalculator.Model.Base.Enums;
using log4net;

namespace ArtOfWar.UnitCalculator.Converter
{
    internal class UnitTypeToUnitImageConverter : IValueConverter
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly string UnitImageUriBase = "pack://application:,,,/ArtOfWar.UnitCalculator;component/App.Resources/Images/Units/";
        private static readonly string UnitImageUriTodo = $"{UnitImageUriBase}todo.png";

        private static readonly Assembly currentAssembly = Assembly.GetExecutingAssembly();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is UnitType unitTypeValue)
            {
                try
                {
                    var uriString = $"{UnitImageUriBase}{unitTypeValue}.png";

                    //Actually this is 20x faster, than throwing Exceptions for not found resource
                    if (ResourceHelper.ResourceExists(currentAssembly, uriString))
                    {
                        return new BitmapImage(new Uri(uriString));
                    }
                    else
                    {
                        return new BitmapImage(new Uri(UnitImageUriTodo));
                    }
                }
                catch (Exception ex)
                {
                    Log.Warn($"Error on Creating Image for <{unitTypeValue}>: {ex.Message}");

                    return null;
                }
            }

            throw new ArgumentException(nameof(value));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
