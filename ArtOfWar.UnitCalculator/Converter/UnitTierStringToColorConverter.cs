﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ArtOfWar.UnitCalculator.Converter
{
    internal class UnitTierStringToColorConverter : IValueConverter
    {
        private readonly UnitTierToStringConverter unitTierToStringConverter = new UnitTierToStringConverter();
        private readonly UnitTierToColorConverter unitTierToColorConverter = new UnitTierToColorConverter();

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var valueEnum = this.unitTierToStringConverter.ConvertBack(value, null, null, null);

            return this.unitTierToColorConverter.Convert(valueEnum, null, null, null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
