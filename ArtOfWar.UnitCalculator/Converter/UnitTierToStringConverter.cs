﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using ArtOfWar.UnitCalculator.Logic.Extensions;
using ArtOfWar.UnitCalculator.Model.Base.Enums;

namespace ArtOfWar.UnitCalculator.Converter
{
    internal class UnitTierToStringConverter : IValueConverter
    {
        private readonly Tier[] valueEnumList = (Tier[]) Enum.GetValues(typeof(Tier));

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Tier tierValue)
            {
                return $"T{(int)tierValue}: {tierValue.GetDescriptionAttribute()}";
            }

            throw new ArgumentException(nameof(value));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string stringValue)
            {
                stringValue = stringValue.Split(": ").Last();

                return this.valueEnumList.FirstOrDefault(x => x.GetDescriptionAttribute().Equals(stringValue));
            }

            throw new ArgumentException(nameof(value));
        }
    }
}
