﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ArtOfWar.UnitCalculator.GUI.Service;
using ArtOfWar.UnitCalculator.GUI.Service.Contract;
using ArtOfWar.UnitCalculator.Logic.Service;
using ArtOfWar.UnitCalculator.Logic.Service.Contracts;
using ArtOfWar.UnitCalculator.Model.Base.Enums;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

namespace ArtOfWar.UnitCalculator.ViewModel
{
    internal class UnitValueViewModel : ViewModelBase
    {
        public class LevelValue
        {
            public Level Level { get; }

            public int ValueT1 { get; set; }
            public int ValueT2 { get; set; }
            public int ValueT3 { get; set; }
            public int ValueT4 { get; set; }

            public LevelValue(Level level)
            {
                this.Level = level;
            }
        }
        public class TierLevelValue
        {
            public Tier Tier { get; }
            public Level Level { get; }
            public int Rank { get; set; }
            public int ValueT1 { get; set; }

            public TierLevelValue(Tier tier, Level level)
            {
                this.Tier = tier;
                this.Level = level;
            }
        }

        private readonly IUnitService unitService;
        private readonly IMessageBoxService messageBoxService;

        public UnitValueViewModel()
            : this(new HardCodedUnitService(), new MessageBoxService())
        {
            if (!this.IsInDesignMode)
            {
                throw new Exception("Empty cTor only in DesignTime...");
            }
        }

        [PreferredConstructor]
        public UnitValueViewModel(IUnitService unitService, IMessageBoxService messageBoxService)
        {
            this.unitService = unitService ?? throw new ArgumentNullException(nameof(unitService));
            this.messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));

            this.InitData();
        }

        private void InitData()
        {
            var levelList = (Level[])Enum.GetValues(typeof(Level));


            var levelValueList = new List<LevelValue>();
            var tierLevelValueList = new List<TierLevelValue>();

            levelValueList.Add(new LevelValue(Level.lvl_1) { ValueT1 = 1 });
            tierLevelValueList.Add(new TierLevelValue(Tier.Normal, Level.lvl_1) { ValueT1 = 1 });

            foreach (var level in levelList.Where(x => !x.Equals(Level.empty)
                                                    && !x.Equals(Level.lvl_1)))
            {
                var previouseLevel = levelValueList.FirstOrDefault(x => (int)x.Level == (int)(level - 1));

                levelValueList.Add(new LevelValue(level) { ValueT1 = previouseLevel.ValueT1 * 2 });
                tierLevelValueList.Add(new TierLevelValue(Tier.Normal, level) { ValueT1 = previouseLevel.ValueT1 * 2 });
            }

            foreach (var level in levelList.Where(x => !x.Equals(Level.empty)))
            {
                var refT1 = levelValueList.First(x => x.Level.Equals(level));

                refT1.ValueT2 = refT1.ValueT1 * 3;
                tierLevelValueList.Add(new TierLevelValue(Tier.Seldom, level) { ValueT1 = refT1.ValueT1 * 3 });

                refT1.ValueT3 = refT1.ValueT1 * 9;
                tierLevelValueList.Add(new TierLevelValue(Tier.Epic, level) { ValueT1 = refT1.ValueT1 * 9 });

                refT1.ValueT4 = refT1.ValueT1 * 27;
                tierLevelValueList.Add(new TierLevelValue(Tier.Legendary, level) { ValueT1 = refT1.ValueT1 * 27 });
            }

            var tmpList = tierLevelValueList.OrderBy(x => x.ValueT1).ToList();

            for (int i = 0; i < tmpList.Count; i++)
            {
                tmpList[i].Rank = i;
            }


            this.TierLevelValueList = new ObservableCollection<TierLevelValue>(tierLevelValueList.OrderBy(x => x.Tier).ThenBy(x => x.Level));
            this.LevelValueList = new ObservableCollection<LevelValue>(levelValueList);
        }

        private ObservableCollection<TierLevelValue> tierLevelValueList;
        public ObservableCollection<TierLevelValue> TierLevelValueList
        {
            get => this.tierLevelValueList;
            set => this.Set(ref this.tierLevelValueList, value);
        }

        private ObservableCollection<LevelValue> levelValueList;
        public ObservableCollection<LevelValue> LevelValueList
        {
            get => this.levelValueList;
            set => this.Set(ref this.levelValueList, value);
        }
    }
}
