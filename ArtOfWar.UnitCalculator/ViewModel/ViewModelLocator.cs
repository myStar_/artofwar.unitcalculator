﻿using System.Reflection;
using System.Windows.Input;
using ArtOfWar.UnitCalculator.Controls;
using ArtOfWar.UnitCalculator.Feature;
using ArtOfWar.UnitCalculator.GUI.Service;
using ArtOfWar.UnitCalculator.GUI.Service.Contract;
using ArtOfWar.UnitCalculator.Logic.Service;
using ArtOfWar.UnitCalculator.Logic.Service.Contracts;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using log4net;

namespace ArtOfWar.UnitCalculator.ViewModel
{
    internal class ViewModelLocator : ViewModelBase
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private bool changeColorFlyoutIsOpen;
        public bool ChangeColorFlyoutIsOpen
        {
            get => this.changeColorFlyoutIsOpen;
            set => this.Set(ref this.changeColorFlyoutIsOpen, value);
        }

        public ICommand ShowChangeColorFlyout => new RelayCommand(execute: () => this.ChangeColorFlyoutIsOpen = true);
        public ICommand ChangeColorFlyoutClosed => new RelayCommand(execute: () => this.ChangeColorFlyoutIsOpen = false);

        public ViewModelLocator()
        {
            SimpleIoc.Default.Register<ITranslator, GoogleFreeTranslator>();
            SimpleIoc.Default.Register<ISettings, GameSettings>();
            SimpleIoc.Default.Register<IUnitService, HardCodedUnitService>();
            SimpleIoc.Default.Register<IMessageBoxService, MessageBoxService>();

            SimpleIoc.Default.Register<UnitOverviewViewModel>();
            SimpleIoc.Default.Register<UnitCompareViewModel>();
            SimpleIoc.Default.Register<UnitGridViewModel>();
            SimpleIoc.Default.Register<BattlefieldGridViewModel>();
            SimpleIoc.Default.Register<UnitValueViewModel>();

            SimpleIoc.Default.Register<UnitDetailRectViewModel>();

            SimpleIoc.Default.Register<MenuColorSettingsViewModel>();
        }

        public UnitOverviewViewModel UnitOverviewViewModel => SimpleIoc.Default.GetInstance<UnitOverviewViewModel>();
        public UnitCompareViewModel UnitCompareViewModel => SimpleIoc.Default.GetInstance<UnitCompareViewModel>();
        public UnitGridViewModel UnitGridViewModel => SimpleIoc.Default.GetInstance<UnitGridViewModel>();
        public BattlefieldGridViewModel BattlefieldGridViewModel => SimpleIoc.Default.GetInstance<BattlefieldGridViewModel>();
        public UnitValueViewModel UnitValueViewModel => SimpleIoc.Default.GetInstance<UnitValueViewModel>();

        public UnitDetailRectViewModel UnitDetailRectViewModel => SimpleIoc.Default.GetInstanceWithoutCaching<UnitDetailRectViewModel>();

        public MenuColorSettingsViewModel MenuColorSettingsViewModel => SimpleIoc.Default.GetInstance<MenuColorSettingsViewModel>();
    }
}
