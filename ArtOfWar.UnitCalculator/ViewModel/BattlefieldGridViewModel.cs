﻿using System;
using ArtOfWar.UnitCalculator.GUI.Service;
using ArtOfWar.UnitCalculator.GUI.Service.Contract;
using ArtOfWar.UnitCalculator.Logic.Service;
using ArtOfWar.UnitCalculator.Logic.Service.Contracts;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

namespace ArtOfWar.UnitCalculator.ViewModel
{
    internal class BattlefieldGridViewModel : ViewModelBase
    {
        private readonly IUnitService unitService;

        public BattlefieldGridViewModel()
            : this(new HardCodedUnitService(), new MessageBoxService())
        {
            if (!this.IsInDesignMode)
            {
                throw new Exception("Empty cTor only in DesignTime...");
            }
        }

        [PreferredConstructor]
        public BattlefieldGridViewModel(IUnitService unitService, IMessageBoxService messageBoxService)
        {
            this.unitService = unitService ?? throw new ArgumentNullException(nameof(unitService));
            this.MessageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));

            //this.InitData();
            //this.InitControlDefaults();
        }

        public IMessageBoxService MessageBoxService { get; }

        //private void InitData()
        //{
        //}

        //private void InitControlDefaults()
        //{
        //}
    }
}
