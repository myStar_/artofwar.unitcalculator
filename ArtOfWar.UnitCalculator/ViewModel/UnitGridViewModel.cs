﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ArtOfWar.UnitCalculator.GUI.Service;
using ArtOfWar.UnitCalculator.GUI.Service.Contract;
using ArtOfWar.UnitCalculator.Logic.Service;
using ArtOfWar.UnitCalculator.Logic.Service.Contracts;
using ArtOfWar.UnitCalculator.Model.Base.Contracts;
using ArtOfWar.UnitCalculator.Model.Base.Enums;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

namespace ArtOfWar.UnitCalculator.ViewModel
{
    internal class UnitGridViewModel : ViewModelBase
    {
        //Still need that kartesian Dummy-Class? Keeping it 
        public class UnitModelAndLevelModel
        {
            private readonly IUnitModel unitModel;

            private readonly IUnitLevelModel unitLevelModel;

            public Tier Tier => this.unitModel.Tier;
            public UnitType UnitType => this.unitModel.UnitType;
            public Level Level => this.unitLevelModel.Level;
            public int HP => this.unitLevelModel.HP;
            public int Offense => this.unitLevelModel.Offense;
            public int Defense => this.unitLevelModel.Defense;
            public int TroopCount => this.unitLevelModel.TroopCount;
            public double Attackspeed => this.unitModel.Attackspeed;
            public bool? HasAreaOfEffect => this.unitModel.HasAreaOfEffect;
            public Skill? Skill => this.unitModel.Skill;
            public double DPS => this.unitLevelModel.DPS;

            public UnitModelAndLevelModel(IUnitModel unitModel, IUnitLevelModel unitLevelModel)
            {
                this.unitModel = unitModel;
                this.unitLevelModel = unitLevelModel;
            }
        }

        private readonly IUnitService unitService;

        public UnitGridViewModel()
            : this(new HardCodedUnitService(), new MessageBoxService())
        {
            if (!this.IsInDesignMode)
            {
                throw new Exception("Empty cTor only in DesignTime...");
            }
        }

        [PreferredConstructor]
        public UnitGridViewModel(IUnitService unitService, IMessageBoxService messageBoxService)
        {
            this.unitService = unitService ?? throw new ArgumentNullException(nameof(unitService));
            this.MessageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));

            this.InitData();
        }

        private void InitData()
        {
            var unitModelAndLevelModelListItems = new List<UnitModelAndLevelModel>();

            foreach (var unit in this.unitService.GetAllUnits())
            {
                foreach (var level in unit.UnitLevels)
                {
                    unitModelAndLevelModelListItems.Add(new UnitModelAndLevelModel(unit, level));
                }
            }

            this.UnitModelAndLevelModelList = new ObservableCollection<UnitModelAndLevelModel>(unitModelAndLevelModelListItems);
        }

        private ObservableCollection<UnitModelAndLevelModel> unitModelAndLevelModelList;
        public ObservableCollection<UnitModelAndLevelModel> UnitModelAndLevelModelList
        {
            get => this.unitModelAndLevelModelList;
            set => this.Set(ref this.unitModelAndLevelModelList, value);
        }
        public IMessageBoxService MessageBoxService { get; }
    }
}
