﻿using System;
using ArtOfWar.UnitCalculator.GUI.Service;
using ArtOfWar.UnitCalculator.GUI.Service.Contract;
using ArtOfWar.UnitCalculator.Logic.Service;
using ArtOfWar.UnitCalculator.Logic.Service.Contracts;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

namespace ArtOfWar.UnitCalculator.ViewModel
{
    internal class UnitCompareViewModel : ViewModelBase
    {
        private readonly IUnitService unitService;

        public UnitCompareViewModel()
            : this(new HardCodedUnitService(), new MessageBoxService())
        {
            if (!this.IsInDesignMode)
            {
                throw new Exception("Empty cTor only in DesignTime...");
            }
        }

        [PreferredConstructor]
        public UnitCompareViewModel(IUnitService unitService, IMessageBoxService messageBoxService)
        {
            this.unitService = unitService ?? throw new ArgumentNullException(nameof(unitService));
            this.MessageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));

            //this.InitData();
            //this.InitControlDefaults();
        }

        public IMessageBoxService MessageBoxService { get; }

        //private void InitData()
        //{
        //    this.unitList = new ObservableCollection<IUnitModel>(this.unitService.GetUnits().OrderBy(x => (int)x.UnitType));
        //}

        //private void InitControlDefaults()
        //{

        //}

        //private ObservableCollection<IUnitModel> unitList;
        //public ObservableCollection<IUnitModel> UnitList
        //{
        //    get => this.unitList;
        //    set => this.Set(ref this.unitList, value);
        //}
    }
}
