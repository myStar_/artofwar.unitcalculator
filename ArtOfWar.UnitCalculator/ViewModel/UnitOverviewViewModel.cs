﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using ArtOfWar.UnitCalculator.GUI.Service;
using ArtOfWar.UnitCalculator.GUI.Service.Contract;
using ArtOfWar.UnitCalculator.Logic.Service;
using ArtOfWar.UnitCalculator.Logic.Service.Contracts;
using ArtOfWar.UnitCalculator.Model.Base.Contracts;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

namespace ArtOfWar.UnitCalculator.ViewModel
{
    internal class UnitOverviewViewModel : ViewModelBase
    {
        private readonly IUnitService unitService;
        private readonly IMessageBoxService messageBoxService;

        public UnitOverviewViewModel()
            : this(new HardCodedUnitService(), new MessageBoxService())
        {
            if (!this.IsInDesignMode)
            {
                throw new Exception("Empty cTor only in DesignTime...");
            }
        }

        [PreferredConstructor]
        public UnitOverviewViewModel(IUnitService unitService, IMessageBoxService messageBoxService)
        {
            this.unitService = unitService ?? throw new ArgumentNullException(nameof(unitService));
            this.messageBoxService = messageBoxService ?? throw new ArgumentNullException(nameof(messageBoxService));
            
            this.InitData();
            //this.InitControlDefaults();
        }

        private void InitData()
        {
            this.UnitList = new ObservableCollection<IUnitModel>(this.unitService.GetAllUnits().OrderBy(x => (int)x.UnitType));
        }

        //private void InitControlDefaults()
        //{
        //}

        private ObservableCollection<IUnitModel> unitList;
        public ObservableCollection<IUnitModel> UnitList
        {
            get => this.unitList;
            set => this.Set(ref this.unitList, value);
        }
    }
}
