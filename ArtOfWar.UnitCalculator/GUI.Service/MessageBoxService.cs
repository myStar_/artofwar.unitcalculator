﻿using System.Windows;
using ArtOfWar.UnitCalculator.GUI.Service.Contract;

namespace ArtOfWar.UnitCalculator.GUI.Service
{
    internal class MessageBoxService : IMessageBoxService
    {
        public MessageBoxResult ShowMessageBox(string messageBoxText)
        {
            return MessageBox.Show(messageBoxText);
        }

        public MessageBoxResult ShowMessageBox(string messageBoxText, string caption)
        {
            return MessageBox.Show(messageBoxText, caption);
        }

        public MessageBoxResult ShowMessageBox(string messageBoxText, string caption, MessageBoxButton messageBoxButton)
        {
            return MessageBox.Show(messageBoxText, caption, messageBoxButton);
        }

        public MessageBoxResult ShowMessageBox(string messageBoxText, string caption, MessageBoxButton messageBoxButton, MessageBoxImage icon)
        {
            return MessageBox.Show(messageBoxText, caption, messageBoxButton, icon);
        }
    }
}
