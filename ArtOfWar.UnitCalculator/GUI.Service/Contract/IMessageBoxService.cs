﻿using System.Windows;

namespace ArtOfWar.UnitCalculator.GUI.Service.Contract
{
    interface IMessageBoxService
    {
        MessageBoxResult ShowMessageBox(string messageBoxText);
        MessageBoxResult ShowMessageBox(string messageBoxText, string caption);
        MessageBoxResult ShowMessageBox(string messageBoxText, string caption, MessageBoxButton messageBoxButton);
        MessageBoxResult ShowMessageBox(string messageBoxText, string caption, MessageBoxButton messageBoxButton, MessageBoxImage icon);
    }
}
