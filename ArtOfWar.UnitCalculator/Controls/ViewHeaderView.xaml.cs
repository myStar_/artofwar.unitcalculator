﻿using System.Windows;
using System.Windows.Controls;

namespace ArtOfWar.UnitCalculator.Controls
{
    /// <summary>
    /// Interaktionslogik für ViewHeader.xaml
    /// </summary>
    public partial class ViewHeaderView : UserControl
    {
        public ViewHeaderView()
        {
             this.InitializeComponent();
        }

        public string HeaderText
        {
            get => (string)this.GetValue(HeaderTextProperty);
            set => this.SetValue(HeaderTextProperty, value);
        }

        public static readonly DependencyProperty HeaderTextProperty =
            DependencyProperty.Register("HeaderText", typeof(string), typeof(ViewHeaderView), new PropertyMetadata(""));


        public string DescriptionText
        {
            get => (string)this.GetValue(DescriptionTextProperty);
            set => this.SetValue(DescriptionTextProperty, value);
        }

        public static readonly DependencyProperty DescriptionTextProperty =
            DependencyProperty.Register("DescriptionText", typeof(string), typeof(ViewHeaderView), new PropertyMetadata(""));
    }
}
