﻿using System.Windows.Media;

namespace ArtOfWar.UnitCalculator.Controls.Model
{
    internal class AppColorData
    {
        public string Name { get; set; }

        public string NameTranslated { get; set; }

        public Brush BorderColorBrush { get; set; }

        public Brush ColorBrush { get; set; }
    }
}