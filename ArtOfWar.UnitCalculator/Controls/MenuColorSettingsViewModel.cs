﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using ArtOfWar.UnitCalculator.Controls.Model;
using ControlzEx.Theming;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using log4net;

namespace ArtOfWar.UnitCalculator.Controls
{
    internal class MenuColorSettingsViewModel : ViewModelBase
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly Application currentApp = Application.Current;
      

        private AppColorData selectedAccentColor;
        public AppColorData SelectedAccentColor
        {
            get => this.selectedAccentColor;
            set
            {
                this.Set(ref this.selectedAccentColor, value);

                if (value != null)
                {
                    this.DoChangeAccent();
                }
            }
        }
        public List<AppColorData> AccentColorList { get; private set; }


        private AppColorData selectedAppTheme;
        public AppColorData SelectedAppTheme
        {
            get => this.selectedAppTheme;
            set
            {
                this.Set(ref this.selectedAppTheme, value);

                if (value != null)
                {
                    this.DoChangeTheme();
                }
            }
        }
        public List<AppColorData> AppThemeList { get; private set; }


        //public MenuColorSettingsViewModel()
        //{
        //    if (!this.IsInDesignMode)
        //    {
        //        throw new Exception("Empty cTor only in DesignTime...");
        //    }
        //}

        [PreferredConstructor]
        public MenuColorSettingsViewModel()
        {
            this.InitData();
            this.InitControlDefaults();
        }

        private void InitData()
        {
            this.AccentColorList = ThemeManager.Current.Themes
                                .GroupBy(x => x.ColorScheme)
                                .OrderBy(a => a.Key)
                                .Select(a => new AppColorData
                                {
                                    Name = a.Key,
                                    ColorBrush = a.First().ShowcaseBrush
                                })
                                .ToList();

            this.AppThemeList = ThemeManager.Current.Themes
                                         .GroupBy(x => x.BaseColorScheme)
                                         .Select(x => x.First())
                                         .Select(a => new AppColorData()
                                         {
                                             Name = a.BaseColorScheme,
                                             BorderColorBrush = a.Resources["MahApps.Brushes.ThemeForeground"] as Brush,
                                             ColorBrush = a.Resources["MahApps.Brushes.ThemeBackground"] as Brush
                                         })
                                         .ToList();
        }

        private void InitControlDefaults()
        {
            var appStyle = ThemeManager.Current.DetectTheme(this.currentApp);

            this.SelectedAccentColor = this.AccentColorList.FirstOrDefault(x => x.Name.Equals(appStyle.ColorScheme));
            this.SelectedAppTheme = this.AppThemeList.FirstOrDefault(x => x.Name.Equals(appStyle.BaseColorScheme));
        }

        private void DoChangeAccent()
        {
            Log.Debug($"Apply Accent <{this.SelectedAccentColor.Name}>");

            ThemeManager.Current.ChangeThemeColorScheme(this.currentApp, this.SelectedAccentColor.Name);
        }

        private void DoChangeTheme()
        {
            Log.Debug($"Apply Theme <{this.SelectedAppTheme.Name}>");

            ThemeManager.Current.ChangeThemeBaseColor(this.currentApp, this.SelectedAppTheme.Name);
        }
    }
}
